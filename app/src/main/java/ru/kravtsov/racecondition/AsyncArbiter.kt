package ru.kravtsov.racecondition

import android.os.AsyncTask
import android.os.Environment
import android.widget.Button
import java.io.File
import java.io.FileWriter
import java.lang.ref.WeakReference


class AsyncArbiter(button: Button,
                   private val count: Int) : AsyncTask<Void, Void, Void>() {
    private var weakButton: WeakReference<Button> = WeakReference(button)

    override fun onPreExecute() {
        super.onPreExecute()
        weakButton.get()?.setText(R.string.btn_text_stop)
        weakButton.get()?.isEnabled = false
    }

    override fun doInBackground(vararg params: Void): Void? {
        try {
            val folder = File("${Environment.getExternalStorageDirectory()}${File.separator}racecondition/")
            if (!folder.exists())
                folder.mkdirs()
            val file = File("${Environment.getExternalStorageDirectory()}${File.separator}racecondition/result.txt")
            if(file.exists())
                file.delete()
            if(file.createNewFile()) {

                    val fileWriter = FileWriter(file)
                    val semaphoreWriter = SemaphoreWriter(fileWriter)
                    val thread1 = Thread(FastRunnable(1, count, fileWriter, semaphoreWriter))
                    val thread2 = Thread(FastRunnable(2, count, fileWriter, semaphoreWriter))
                    thread1.start()
                    thread2.start()

                    thread1.join()
                    thread2.join()

                    fileWriter.close()

            }
        }catch (e: Exception) {
            System.out.print(e.toString())
        }
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        weakButton.get()?.setText(R.string.btn_text_start)
        weakButton.get()?.isEnabled = true
    }
}