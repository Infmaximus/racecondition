package ru.kravtsov.racecondition

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v4.app.ActivityCompat


class MainActivity : AppCompatActivity() {

    companion object {
        const val WRITE_CODE = 1
        const val COUNT_SIZE = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val permissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
                    startRace()
                } else {
                    ActivityCompat.requestPermissions(this, arrayOf((Manifest.permission.WRITE_EXTERNAL_STORAGE)),WRITE_CODE)
                }
            }else
                startRace()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            WRITE_CODE -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startRace()
                }
                return
            }
        }
    }

    private fun startRace(){
        AsyncArbiter(button,COUNT_SIZE).execute()
    }
}
