package ru.kravtsov.racecondition

import java.io.*

class FastRunnable(private val numberThread:Int,
                   private val count:Int,
                   private val fileWriter: FileWriter,
                   private val semaphoreWriter: SemaphoreWriter) :Runnable{

    override fun run() {
        try {
            if(count>0) {
                for (i in 1..count) {
                    fileWriter.write("Поток $numberThread: текущее значение $i\n\n")
                }
                semaphoreWriter.writerEnd(numberThread)
            }
        }catch (e:IOException){
            System.out.print(e.toString())
        }
    }
}