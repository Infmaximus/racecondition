package ru.kravtsov.racecondition

import java.io.FileWriter

class SemaphoreWriter(private val fileWriter: FileWriter){

    private var trigger = 0

    @Synchronized
    fun writerEnd(numThread:Int) {
        val text = "Поток $numThread: закончил ${if(trigger==0)"первым" else "вторым"}\n\n"
            try {
                fileWriter.write(text)
            }catch (e: Exception) {
                System.out.print(e.toString())
            }
        trigger++
    }

}